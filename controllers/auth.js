const { response } = require("express");
const Usuario = require("../models/usuario");
const bcryptjs = require("bcryptjs");
const bcrypt = require("bcrypt");
const {generarJWT} = require('../helpers/jwt')

const login = async (req, res = response) => {
  const { email, password } = req.body;
  try {
    //Verificar email
    const usuarioDB = await Usuario.findOne({ email });
    if (!usuarioDB) {
      return res.status(404).json({
        ok: false,
        msg: "El email o la contraseña no es correcto",
      });
    }
    //Verificar contraseña
    const validPassword = bcryptjs.compareSync(password, usuarioDB.password);
    if (!validPassword) {
      return res.status(400).json({
        ok: false,
        msg: "El email o la contraseña no es correcto",
      });
    }

    //Generar el token - JWT
    const token = await generarJWT(usuarioDB.id);
    console.log('aqui '+usuarioDB.id);
    res.json({
      ok: true,
      token,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: "Erro inesperado...revisar logs",
    });
  }
};

module.exports = { login };
