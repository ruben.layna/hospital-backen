const Usuario = require("../models/usuario");
const { response } = require("express");
const bcryptjs = require("bcryptjs");
const bcrypt = require("bcrypt");
const { generarJWT } = require("../helpers/jwt");

const getUsuarios = async (req, res) => {
  const desde = Number(req.query.desde) || 0;
  // const usuarios = await Usuario.find({}, "nombre email role google")
  //   .skip(desde)
  //   .limit(10);
  // const total = await Usuario.countDocuments();
  const[usuarios, total] = await Promise.all([
    Usuario.find({}, "nombre email role google").skip(desde).limit(10),
    Usuario.countDocuments(),
  ]);
  res.json({
    ok: true,
    usuarios,
    uid: req.uid,
    total,
  });
};

const CreaUsuarios = async (req, res = response) => {
  const { email, password, nombre } = req.body;

  try {
    const existeEmail = await Usuario.findOne({ email });

    if (existeEmail) {
      return res.status(400).json({
        ok: false,
        msg: "Ya existe un usuario con este email en nuestra base de datos",
      });
    }

    const usuario = new Usuario(req.body);
    ///Encriptar contraseña
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync(password, salt);

    ////Guaradar usuario
    await usuario.save();
    ////Generar token
    const token = await generarJWT(usuario.id);
    res.json({
      ok: true,
      usuario,
      token,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: "Erro inesperado...revisar logs",
    });
  }
};

const actUsuario = async (req, res = response) => {
  //////Validar token y comprobar que el usuario sea el correcto ...pendiente

  const uid = req.params.id;
  try {
    const usuarioDB = await Usuario.findById(uid);
    if (!usuarioDB) {
      return res.status(404).json({
        oK: false,
        msg: "No existe un usuarion con ese id",
      });
    }
    //////Actualizar////////////
    const { password, google, email, ...campos } = req.body;
    if (usuarioDB.email !== email) {
      const existeEmail = await Usuario.findOne({ email });
      if (existeEmail) {
        return res.status(400).json({
          ok: false,
          msg: "Ya existe un usuario con este email en nuestra base de datos",
        });
      }
    }
    // delete campos.password;
    // delete campos.google;
    campos.email = email;
    const usuarioAct = await Usuario.findByIdAndUpdate(uid, campos, {
      new: true,
    });
    res.json({
      ok: true,
      usuario: usuarioAct,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: "Erro inesperado...revisar logs",
    });
  }
};

/////Eliminar usuario///////
const borrarUsuario = async (req, res = response) => {
  const uid = req.params.id;
  try {
    const usuarioDB = await Usuario.findById(uid);
    if (!usuarioDB) {
      return res.status(404).json({
        oK: false,
        msg: "No existe un usuarion con ese id",
      });
    }
    //ELiminar
    await Usuario.findByIdAndDelete(uid);
    res.json({
      ok: true,
      msg: "Usuario eliminado correctamente...",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: "Erro inesperado...revisar logs",
    });
  }
};

module.exports = { getUsuarios, CreaUsuarios, actUsuario, borrarUsuario };
