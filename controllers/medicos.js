const { response } = require("express");
const Medico = require("../models/medicos");

const getMedicos = async (req, res = response) => {
  const medicos = await Medico.find()
    .populate("usuario", "nombre img")
    .populate("hospital", "nombre img");
  res.json({
    ok: true,
    medicos,
  });
};

const crearMedicos = async (req, res = response) => {
  const uid = req.uid;
  const medico = new Medico({ usuario: uid, ...req.body });
  try {
    const medicoDB = await medico.save();
    res.json({
      ok: true,
      medico: medicoDB,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: "Erro inesperado...revisar logs",
    });
  }
};

const actMedicos = (req, res = response) => {
  res.json({
    ok: true,
    msg: "actMedicos",
  });
};

const eliminarMedicos = (req, res = response) => {
  res.json({
    ok: true,
    msg: "eliminarHospital",
  });
};
module.exports = { getMedicos, crearMedicos, actMedicos, eliminarMedicos };
