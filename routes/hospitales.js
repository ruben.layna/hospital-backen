// /api/hospitales

const { Router } = require("express");
const router = Router();
const { check } = require("express-validator");
const { validarCampos } = require("../middlewares/validar-campos");
const { ValidarJWT } = require("../middlewares/validadr-jwt");
const {getHospitales, crearHospital, actHospital, eliminarHospital} = require('../controllers/hospitales');

router.get("/",  ValidarJWT, getHospitales);

router.post(
  "/",
  [
    ValidarJWT,
    check('nombre','el nombre del Hopsital es obligatorio').not().isEmpty(),
    validarCampos
  ],
  crearHospital
);

router.put(
  "/:id",
  [
   
  ],
  actHospital
);

router.delete("/:id", eliminarHospital);

module.exports = router;
