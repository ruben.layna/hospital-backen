// Ruta: /api/usuarios

const { Router } = require("express");
const router = Router();
const { check } = require("express-validator");
const { validarCampos } = require("../middlewares/validar-campos");
const {
  getUsuarios,
  CreaUsuarios,
  actUsuario,
  borrarUsuario,
} = require("../controllers/usuarios");
const { ValidarJWT } = require("../middlewares/validadr-jwt");

router.get("/", ValidarJWT, getUsuarios);

router.post(
  "/",
  [
    check("nombre", "Falta agregar nombre").not().isEmpty(),
    check("password", "Falta agregar contraseña").not().isEmpty(),
    check("email", "Falta agregar email").isEmail(),
    validarCampos,
  ],
  CreaUsuarios
);

router.put(
  "/:id",
  [
    ValidarJWT,
    check("nombre", "Falta agregar nombre").not().isEmpty(),
    // check("password", "Falta agregar contraseña").not().isEmpty(),
    check("role", "Falta agregar rol").not().isEmpty(),
    check("email", "Falta agregar email").isEmail(),
    validarCampos,
  ],
  actUsuario
);

router.delete("/:id", ValidarJWT, borrarUsuario);

module.exports = router;
