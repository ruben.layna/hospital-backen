// /api/medicos
const { Router } = require("express");
const router = Router();
const { check } = require("express-validator");
const { validarCampos } = require("../middlewares/validar-campos");
const { ValidarJWT } = require("../middlewares/validadr-jwt");
const {
  getMedicos,
  crearMedicos,
  actMedicos,
  eliminarMedicos,
} = require("../controllers/medicos");

router.get("/", ValidarJWT, getMedicos);
router.post(
  "/",
  [
    ValidarJWT,
    check("nombre", "el nombre del Medico es obligatorio").not().isEmpty(),
    check("hospital", "el hospital id debe de ser valido").isMongoId(),
    validarCampos,
  ],
  crearMedicos
);

router.put("/:id", [], actMedicos);

router.delete("/:id", eliminarMedicos);

module.exports = router;
