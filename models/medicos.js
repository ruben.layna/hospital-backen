const { Schema, model, Collection } = require("mongoose");

const MedicosSchema = Schema({
  nombre: { type: String, required: true },
  img: { type: String },
  usuario: {require: true, type: Schema.ObjectId, ref: 'Usuario'},
  hospital: {require: true, type: Schema.ObjectId, ref: 'Hospital'}
});

MedicosSchema.method('toJSON', function () {
  const { __v, ...object } = this.toObject();
  return object;
});

module.exports = model("Medico", MedicosSchema);